import React from 'react';
import uuidv4 from 'uuid/v4';
import MessageList from '../../components/MessageList';
import MessageInput from '../MessageInput';
import { Select, Input } from 'semantic-ui-react';
import styles from './styles.module.scss';

const MESSAGES_PER_PAGE = 7;

class App extends React.Component {
    constructor(props) {
        super(props);

        const userId = localStorage.getItem('userId');
        if (!userId) localStorage.setItem('userId', uuidv4());

        this.state = {
            buttonText: 'Send',
            currentUserId: localStorage.getItem('userId'),
            filter: '',
            skip: 0,
            first: MESSAGES_PER_PAGE,
            orderBy: 'createdAt_ASC'
        };
    }

    onPageChange = (event, { activePage }) => {
        console.log(activePage, (activePage - 1) * MESSAGES_PER_PAGE);
        this.setState({
            skip: (activePage - 1) * MESSAGES_PER_PAGE
        });
    };

    onSortChange = (event, data) => this.setState({ orderBy: data.value });

    onFilterChange = (event) => this.setState({ filter: event.target.value });

    render() {
        const { filter, skip, first, orderBy } = this.state;
        const sortOrders = [
            /*{ key: 'likeCount_DESC', value: 'likeCount_DESC', text: 'By likes in descending order'},
            { key: 'likeCount_ASC', value: 'likeCount_ASC', text: 'By likes in ascending order'},
            { key: 'dislikeCount_DESC', value: 'dislikeCount_DESC', text: 'By dislikes in descending order'},
            { key: 'dislikeCount_ASC', value: 'dislikeCount_ASC', text: 'By dislikes in ascending order'},*/
            { key: 'createdAt_DESC', value: 'createdAt_DESC', text: 'By posted time in descending order'},
            { key: 'createdAt_ASC', value: 'createdAt_ASC', text: 'By posted time in ascending order'}
        ];

        return (
            <div className={styles.container}>
                <div className={styles.filters}>
                    <Select placeholder="Select the sorting order" options={sortOrders} onChange={this.onSortChange} />
                    <Input placeholder="Input message text" defaultValue={filter} onChange={this.onFilterChange}/>
                </div>
                <MessageList filter={filter} skip={skip} first={first} messagePerPage={MESSAGES_PER_PAGE} orderBy={orderBy} onPageChange={this.onPageChange} />
                <MessageInput filter={filter} skip={skip} first={first} orderBy={orderBy} />
            </div>
        );
    }
}

export default App;
