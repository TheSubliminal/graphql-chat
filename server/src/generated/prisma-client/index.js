"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var prisma_lib_1 = require("prisma-client-lib");
var typeDefs = require("./prisma-schema").typeDefs;

var models = [
  {
    name: "Message",
    embedded: false
  },
  {
    name: "MessageReaction",
    embedded: false
  }
];
exports.Prisma = prisma_lib_1.makePrismaClientClass({
  typeDefs,
  models,
  endpoint: `https://eu1.prisma.sh/michael-fridrikhson-8e634f/bsa2019-graphql-task/dev`
});
exports.prisma = new exports.Prisma();
