const postMessage = (parent, args, context) => {
    const { userId, text, replyTo } = args;

    return context.prisma.createMessage({
        userId, text, replyTo
    });
};

const setReaction = async (parent, args, context) => {
    const { userId, isLike, messageId } = args;
    const reactionExists = await context.prisma.$exists.messageReaction({ userId, message: { id: messageId } });

    if (reactionExists) {
        const reactions = await context.prisma.messageReactions({ where: { userId, message: { id: messageId } }});
        const reaction = reactions[0];

        if (reaction.isLike === isLike) {
            await context.prisma.deleteMessageReaction({ id: reaction.id });
        } else {
            await context.prisma.updateMessageReaction({
                data: { isLike: !reaction.isLike },
                where: { id: reaction.id }}
            );
        }
    } else {
        await context.prisma.createMessageReaction({
            userId,
            isLike,
            message: {
                connect: { id: messageId }
            }
        });
    }

    return context.prisma.message({ id: messageId });
};

module.exports = { postMessage, setReaction };